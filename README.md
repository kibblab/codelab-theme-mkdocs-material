# Codelabs Theme for MkDocs Material

[See demo site.](https://kibblab.gitlab.io/codelab-theme-mkdocs-material/)

⚠ Still under construction.

A derivation of [Hugo Codelab Themes](https://hugothemesfree.com/tag/codelabs/) (which are copies of [Google Codelabs](https://github.com/googlecodelabs/tools)), which can integrate and look consistent with [MkDocs Material Theme](https://squidfunk.github.io/mkdocs-material/) in some multi-repo build scheme.

TODO:

- Save scroll state
- Calculate duration

## Setup

1. Clone this repo
2. cd into newly cloned repo
3. `pip install -r requirements.txt`
4. Run `./update.sh` or `.\update.ps1`

The `update` script must be run every time you change versions of Mkdocs Material.

## Config

**mkdocs.yml** needs to have certain settings included:

```yaml
plugins:
  - search
  - codelab

theme:
  name: material
  custom_dir: codelab # this override directory
  report_bugs: https://www.example.com # url for the Bug Report button
  global: false # Whether to automatically make non-index pages a codelab. Default false

markdown_extensions:
  - attr_list
```

## Writing a Codelab

To enable the codelab page, in the YAML meta, there needs to be a property `codelab: true`.

All H2 Headings are automatically used as steps, and numbers are added automatically.

Using [attr-list](https://python-markdown.github.io/extensions/attr_list/), you can add durations to each step which will automatically be summed in total.

```markdown
---
codelab: true
title: "My Codelab"
author: "Anonymous Mouse"
revision_date: 2021-01-08
---

## Do task {: duration="2:00" }

Blah blah 2 minute task

### Subtask

![Alt text for accessibility](an/immage.jpg)

## Do another task {: duration="5:00:00" }

Blah blah... 5 hour task

```

## Codelab Plugin Config

```yaml
plugins:
  - codelab:
      # HTML tag used to distinguish between step passages
      delimiter: 'h2' # default 'h2' which corresponds to Markdown H2 (##)
      auto_number: True # Whether to auto-number sections
```

## Development

### Python Virtual Envrionment

```shell
# Create 
python -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
```

### Localization

Go to `codelab/partials/languages` folder and add your language by copying the file from the original source repository at `mkdocs-material/src/partials/languages`.

Keys:

```
"codelab.exit": "Exit to Home"
```

## GitLab Pages (Public Site)

```
rm -r public
sed -i 's/direction: ltr/direction: rtl/' mkdocs.yml
mkdocs build
cp public/guide/index.html rtl.html

sed -i 's/direction: rtl/direction: ltr/' mkdocs.yml
mkdocs build
cp rtl.html public/guide/rtl.html
rm rtl.html
```